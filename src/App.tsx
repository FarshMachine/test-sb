import React from 'react'
import { ApolloProvider, ApolloClient, InMemoryCache } from '@apollo/client'
import View from './View'

const client = new ApolloClient({
  uri: 'https://rickandmortyapi.com/graphql',
  cache: new InMemoryCache()
})

const App: React.FC = () => {
  return (
    <ApolloProvider client={client}>
      <View />
    </ApolloProvider>
  );
}

export default App
