import styled from 'styled-components'

export default styled.div`
	width: 810px;
	height: 100%;
	display: flex;
	flex-direction: column;
	margin: auto;
	padding-top: 100px;
	box-sizing: border-box;
	/* background-color: #ececec; */
`