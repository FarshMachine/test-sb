import styled from 'styled-components'

type ImageProps = {
	src: string
}

export const Party = styled.div`
	position: fixed;
	display: flex;
	width: 100%;
	left: 0;
	bottom: 100px;
	flex-direction: column;
	align-items: center;
	font-size: 30px;
	flex-weight: bold;
`

export const Cards = styled.div`
	display: flex;
	justify-content: center;
`

export const Card = styled.div`
	display: flex;
	position: relative;
	width: 180px;
	height: 180px;
	padding-bottom: 20px;
	margin-right: 20px;
	box-sizing: border-box;
	justify-content: center;
	align-items: flex-end;
	background-color: #DADADA;
	color: white;
	font-size: 24px;

`

export const Image = styled.div`
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: url(${(props: ImageProps) => props.src}) center no-repeat;
	background-size: contain;
`