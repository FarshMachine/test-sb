import React from 'react'
import { Party, Cards, Card, Image } from './style'

type PartyProps = {
	rickImage: string
	mortyImage: string
}

const PartyComponent: React.FC<PartyProps> = ({
	rickImage,
	mortyImage
}): JSX.Element => {
	return (
		<Party>
			PARTY
			<Cards>
				<Card>
					<Image src={rickImage} />
					RICK
				</Card>
				<Card>
					<Image src={mortyImage} />
					MORTY
				</Card>
			</Cards>
		</Party>
	)
}

export default React.memo(PartyComponent)