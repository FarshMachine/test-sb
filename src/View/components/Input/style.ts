import styled from 'styled-components'

export default styled.input`
	width: 100%;
	height: 80px;
	margin-bottom: 30px;
	font-size: 30px;
	box-sizing: border-box;
`