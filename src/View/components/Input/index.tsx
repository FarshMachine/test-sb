import React from 'react'
import StyledInput from './style'

type InputProps = {
	searchString: string
	setSearchString: (value: string) => void
}

const InputComponent: React.FC<InputProps> = ({
	setSearchString,
	searchString
}): JSX.Element => {
	const changeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
		setSearchString(event.target.value)
	}

	return (
		<StyledInput value={searchString} onChange={changeHandler}/>
	)
}

export default React.memo(InputComponent)