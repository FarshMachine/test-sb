import React, { useState, useCallback } from 'react'
import Images from './style'
import { useQueryCharacters } from './hooks'
import Image from './Image'

type BanImageType = (prevImage: string) => string

type ImagesProps = {
	searchString: string
	setRickImage: (image: string | BanImageType) => void
	setMortyImage: (image: string | BanImageType) => void
}

const ImagesComponent: React.FC<ImagesProps> = ({
	searchString,
	setRickImage,
	setMortyImage
}): JSX.Element => {
	const [bannedImages, setBannedImages] = useState<Array<string>>([])
	const { data, loading } = useQueryCharacters(searchString)
	
	const banImage = useCallback((id: string) => {
		setBannedImages((bannedImages) => {
			return [...bannedImages, id]
		})
	}, [])

	const setPartyImage = useCallback((name: string, image: string) => {
		const lowerCaseName = name.toLowerCase()

		if (lowerCaseName.includes('rick')) {
			setRickImage(image)
		}

		if (lowerCaseName.includes('morty')) {
			setMortyImage(image)
		}
	}, [setRickImage, setMortyImage])

	return (
		<Images>
			{loading ? (
				<div>Loading...</div>
			) : (
				<>
					{data && data.characters.results.slice(0, 8).map((fields) => {
						if (bannedImages.includes(fields.id)) return null
						
						return (
							<Image
								key={fields.id}
								banImage={banImage}
								setPartyImage={setPartyImage}
								{...fields}
							/>
						)
					})}
				</>
			)}
		</Images>
	)
}

export default React.memo(ImagesComponent)