import React from 'react'
import { Image, BanButton } from './style'

type ImageProps = {
	id: string
	image: string
	name: string
	setPartyImage: (id: string, image: string) => void
	banImage: (id: string) => void
}

const ImageComponent: React.FC<ImageProps> = ({
	image,
	id,
	name,
	setPartyImage,
	banImage
}): JSX.Element => {
	const banButtonClickHandler = (e: React.MouseEvent<HTMLElement>) => {
		e.stopPropagation()
		banImage(id)
	}

	return (
		<Image onClick={() => setPartyImage(name, image)} src={image}>
			<BanButton onClick={banButtonClickHandler}>X</BanButton>
		</Image>
	)
}

export default React.memo(ImageComponent)