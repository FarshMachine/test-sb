import styled from 'styled-components'

type ImageProps = {
	src: string
}

export const Image = styled.div`
	position: relative;
	width: 180px;
	height: 180px;
	background: url(${(props: ImageProps) => props.src}) center no-repeat;
	background-size: cover;
	margin-bottom: 30px;
	margin-right: 30px;

	:nth-child(4n){
		margin-right: 0;
	}
`

export const BanButton = styled.div`
	position: absolute;
	width: 30px;
	height: 30px;
	right: 10px;
	top: 10px;
	background-color: #ececec;
	border-radius: 50%;
	line-height: 30px;
	vertical-align: middle;
	text-align: center;
	cursor: pointer;
`