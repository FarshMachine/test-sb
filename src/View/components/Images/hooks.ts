import { useEffect } from 'react'
import { useLazyQuery, gql } from '@apollo/client'


interface Character {
	id: string
	image: string
	name: string
}

interface Characters {
	characters: {
		results: Character[]
	}
}

type CharacterVariables = {
	name: string
}

type Hook = (searchString: string) => {
	loading: boolean
	data: Characters | undefined
}

const GET_CHARACTERS_BY_NAME = gql`
	query GetCharactersByName($name: String!) {
		characters(page: 1, filter: { name: $name }) {
			results {
				id
				image
				name
			}
		}
	}
`

export const useQueryCharacters: Hook = (
	searchString
) => {
	const [getCharacters, { loading, data }] = useLazyQuery<Characters, CharacterVariables>(
		GET_CHARACTERS_BY_NAME
	)

	useEffect(() => {
		setTimeout(() => {
			if(searchString.length <= 2) return
			getCharacters({ variables: { name: searchString } })
		}, 300)
	}, [searchString, getCharacters])

	return { loading, data }
}