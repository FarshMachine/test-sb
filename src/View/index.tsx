import React, { useState } from 'react'
import View from './style'
import Input from './components/Input'
import Images from './components/Images'
import Party from './components/Party'

const ViewComponent: React.FC = (): JSX.Element => {
	const [searchString, setSearchString] = useState<string>('')
	const [rickImage, setRickImage] = useState<string>('')
	const [mortyImage, setMortyImage] = useState<string>('')

	return (
		<View>
			<Input
				searchString={searchString}
				setSearchString={setSearchString}
			/>
			<Images
				searchString={searchString}
				setRickImage={setRickImage}
				setMortyImage={setMortyImage}
			/>
			<Party
				rickImage={rickImage}
				mortyImage={mortyImage}
			/>
		</View>
	)
}

export default ViewComponent